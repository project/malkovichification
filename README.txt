SUMMARY
-------

Inspired by a scene from the film "Being John Malkovich," the
Malkovichification module transforms all rendered text on all non-admin pages
of the website to repetitions of the word "Malkovich" (or another word of your
choosing). The module preserves the markup and styling of the page when 
Malkovichifying the output.

This module was not originally designed for practical purposes but can be used
to replace the text on a local copy of a live site in order to present or 
analyze the site with neutral text.

REQUIREMENTS
------------

None.


INSTALLATION
------------

* Install as usual. See http://drupal.org/node/895232 for further information.

* Note that installing the module does not inherently activate
  Malkovichification. Malkovichification must be explicitly activated in
  configuration.


CONFIGURATION
-------------

* Configure user permissions in Administration » People » Permissions

* Activate and configure the module at Administration » Configuration » Search
  and metadata » Malkovichification


IN CASE OF DISASTER
-------------------

If Malkovichification goes awry on your site, and you cannot disable the module
or cannot find the configuration page, first try deselecting the checkbox at
/admin/config/search/malkovich.

If that does not work, simply remove the module from the /sites/all/modules
directory.


CONTACT
-------

Author and maintainer:
* Robert C. Ring - http://drupal.org/u/rcr1000
